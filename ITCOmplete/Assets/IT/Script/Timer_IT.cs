﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer_IT : MonoBehaviour {
	public ProgressBar_IT progress;

	Image FillImg;
	public float time;
	float TimeStart = 20;
	public ParticleSystem Ngelu;

	bool Ada = false;

	void Start () {
		FillImg = this.GetComponent<Image> ();
		time = TimeStart;
	}
	
	// Update is called once per frame
	void Update () {
		if (time >= 0) {
			time -= Time.deltaTime;
			FillImg.fillAmount = time / TimeStart;
			if (time <= 7) {
				if (Ada == false) {
					Ngelu.Play ();
					Ada = true;
				}
			} else {
				if (Ada == true) {
					Ngelu.Stop ();
					Ada = false;
				}
			}
		}
		else {
			Application.LoadLevel (0);
		}
	}
}
