﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake_IT : MonoBehaviour {

	public float ShakeTimer;
	public float ShakeAmount;

	// Use this for initialization
	void Start () {
		
	}
		
	
	// Update is called once per frame
	void Update () {
		if (ShakeTimer >= 0) {
			Vector2 ShakePos = Random.insideUnitCircle * ShakeAmount;

			transform.position = new Vector3 (transform.position.x + ShakePos.x, transform.position.y + ShakePos.y, transform.position.z);
				
			ShakeTimer -= Time.deltaTime;
		} else {
			transform.position = new Vector3 (2.85f,4.98f,-10f);
		}
	}

	public void ShakeCamera(float ShakePwr,float ShakeDur){
		ShakeAmount = ShakePwr;
		ShakeTimer = ShakeDur;
	}

}
