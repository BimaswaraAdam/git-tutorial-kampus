﻿using UnityEngine;
using System.Collections;

public class Generate_Comp : MonoBehaviour {

    public Component_Comp[] component;
    public Quest_Comp[] quest;
    private float generateComponenetDelayCount;
    public bool generateQuest_Comp = false;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
        generateComponenetDelayCount -= Time.deltaTime;
        if (generateComponenetDelayCount <= 0)
        {
            Instantiate(component[Random.Range(0,component.Length)], new Vector2 (-4, -3.62f ), Quaternion.identity);
            generateComponenetDelayCount = 2.0f;
        }

        if (generateQuest_Comp == false)
        {
            Instantiate(quest[Random.Range(0, component.Length)], new Vector2(1.2f, 2.6f), Quaternion.identity);
            generateQuest_Comp = true;
        }
	}
}
