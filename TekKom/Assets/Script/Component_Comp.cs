﻿using UnityEngine;
using System.Collections;

public class Component_Comp : MonoBehaviour {

    public SpringJoint2D seret_Comp;
    public float CurPos_Comp;
    public Generate_Comp Quest;
    public Quest_Comp objek;
    public float progress = 0.0f;


    // Use this for initialization
    void Start()
    {
        CurPos_Comp = transform.position.x;
        objek = FindObjectOfType<Quest_Comp>().GetComponent<Quest_Comp>();
        Quest = FindObjectOfType<Generate_Comp>().GetComponent<Generate_Comp>();
    }
	// Update is called once per frame
	void Update () {
        CurPos_Comp += 0.01f;
        this.transform.Translate(Vector2.right * 0.01f);        
        if (this.transform.position.x > 4)
        {
            Destroy(this.gameObject);
        }
	
	}

    void Awake()
    {
        seret_Comp = this.gameObject.GetComponent<SpringJoint2D>();
        seret_Comp.connectedAnchor = gameObject.transform.position;
    }

    void OnMouseDown()
    {
        seret_Comp.enabled = true;
        print("mouse down");
    }

    void OnMouseDrag()
    {
        if(seret_Comp.enabled = true)
        {
            Vector2 cursorPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            this.gameObject.transform.position = cursorPosition;
            print("mouse drag");
        }
    }

    void OnMouseUp()
    {
        seret_Comp.enabled = false;
        print("mouse up");

       transform.position = new Vector2(CurPos_Comp, -3.62f);
    }

    void OnCollisionEnter2D(Collision2D Zone)
    {
        if (Zone.transform.tag == "Zone")
        {
            if (this.transform.tag == objek.transform.tag)
            {
                print(objek.transform.tag);
                print("Destroy");
                progress = progress + 1.0f;
                Destroy(this.gameObject);
                Quest.generateQuest_Comp = false;
            }
        }
    }
}
