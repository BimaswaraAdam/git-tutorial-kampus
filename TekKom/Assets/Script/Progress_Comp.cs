﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Progress_Comp : MonoBehaviour {
    public Component_Comp bar;
	// Use this for initialization
	void Start () {
        bar = FindObjectOfType<Component_Comp>().GetComponent<Component_Comp>();
    }
	
	// Update is called once per frame
	void Update () {
        float current = bar.progress * 1.0f / 100;
        this.GetComponent<Image>().fillAmount = current;
	}
}
