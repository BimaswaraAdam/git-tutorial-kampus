﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bola = MVCtus.Model;

namespace MVCtus
{
    
    public class Controller
    {
        public static int TempPos;
        public static void GerakKiri()
        {
            Bola.BallPos--;
            SetPos(Bola.BallPos);
        }

        public static void GerakKanan()
        {
            Bola.BallPos++;
            SetPos(Bola.BallPos);
        }

        public static void SetPos(int x)
        {
            TempPos = x;
        }
    }

}
