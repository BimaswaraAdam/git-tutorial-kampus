﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ProgressBar : MonoBehaviour {

    public UnityEngine.UI.Image PBar;
    public Click click;
    public GameObject bar;
    public int max = 100;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        // bar.transform.localScale = new Vector2 (click.Progress, bar.transform.localScale.y);
        float current = click.Progress*1.0f / 100;
        print(current);
        this.GetComponent<Image>().fillAmount = current;
	}
}
