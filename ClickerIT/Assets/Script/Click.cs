﻿using UnityEngine;
using System.Collections;

public class Click : MonoBehaviour {

    public UnityEngine.UI.Text Progression;
    public UnityEngine.UI.Text DeadLine;
    public int Progress;
    public int Deadline;
    public int PerClicked;

    float decreaseFactor = 1.0f;
    float shakeAmount = 0.7f;
    float shake = 0f;
    //Camera = var camera;


    
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        Progression.text = "Progress" + Progress;
    }

    public void Clicked()
    {
        Progress += PerClicked;
        Camera.main.transform.position = Random.insideUnitSphere * 2.1f;
        //camera.transform.localPosition = Random.insideUnitSphere * decreaseFactor;
        //shake -= Time.deltaTime * decreaseFactor;
    }
}
