﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressBar_IT : MonoBehaviour {

	public Touch_IT Click;
	public Timer_IT Timer;

	public CameraShake_IT Getar;
	public Text ScoreText;
	public int Score = 0;
	public GameObject Bar;
	public ParticleSystem Hore;
	int Nilai = 5;
	public float Total;

	// Use this for initialization
	void Start () {
		ScoreText.text = Score.ToString ("0");
	}

	// Update is called once per frame
	void Update () {
		Total = Click.Progress * 1f / Nilai;
		this.GetComponent<Image> ().fillAmount = Total;
		if (Total > 1) {
			SetNormal ();
			if (Timer.time <= 17) {
				Timer.time += 2;
			} else {
				Timer.time += 1;
			}
			Handheld.Vibrate ();
			Nilai += 2;
			Score += 1;
			Hore.Play ();
			Getar.ShakeCamera (0.1f, 0.5f);
			ScoreText.text = Score.ToString ();
		}
	}

	public void SetNormal()
	{
		Click.Progress = 0;
		Total = 0f;
		this.GetComponent<Image>().fillAmount = Total;
	}


}
